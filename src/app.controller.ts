import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/summary/:id')
  getSummaryData(@Param('id') summaryId: string) {
    console.info({
      controller: 'app',
      method: 'GET',
      endpoint: '/summary/:id',
    });
    return this.appService.getSummaryData(summaryId);
  }

  @Post('/summary/email')
  sendSummaryEmail(@Body() body: any) {
    console.info({
      controller: 'app',
      method: 'POST',
      endpoint: '/summary/email',
    });
    return this.appService.sendSummaryEmail(body.userId, body.saveSummary);
  }

  @Get('/users/:id')
  getUserData(@Param('id') userId: string) {
    console.info({
      controller: 'app',
      method: 'GET',
      endpoint: '/users/:id',
    });
    return this.appService.getUserData(userId);
  }

  @Patch('/users/:id')
  updateUser(@Param('id') userId: string, @Body() body: any) {
    console.info({
      controller: 'app',
      method: 'PATCH',
      endpoint: '/users/:id',
    });
    return this.appService.updateUser(userId, body);
  }
}
