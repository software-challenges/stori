export interface Transaction {
  id: string;
  date: string;
  transaction: number;
}
