export default () => ({
  smtp: {
    host: process.env.HOST,
    port: parseInt(process.env.PORT, 10),
    mail: process.env.MAIL,
    pass: process.env.PASS,
  },
  mongodb: {
    url: process.env.MONGODB_URL,
  },
  images: {
    logo: process.env.STORI_LOGO_URL,
    frontPage: process.env.STORI_FRONT_PAGE_URL,
  },
});
