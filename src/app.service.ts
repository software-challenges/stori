import { Injectable } from '@nestjs/common';
import { EmailService } from './email/services/email.service';
import { FilesManagerService } from './files-manager/services/files-manager.service';
import { UsersService } from './users/services/user.service';

@Injectable()
export class AppService {
  constructor(
    private _emailService: EmailService,
    private _summaryServices: FilesManagerService,
    private _userService: UsersService,
  ) {}

  sendSummaryEmail(userId: string, saveSummary: boolean) {
    console.info('appService, sendSummaryEmail');
    return this._emailService.sendSummaryEmailInformation(userId, saveSummary);
  }

  getSummaryData(summaryId: string) {
    console.info('appService, getSummaryData');
    return this._summaryServices.getSummaryData(summaryId);
  }

  getUserData(userId: string) {
    console.info('appService, getUserData');
    return this._userService.getUser(userId);
  }

  updateUser(userId: string, data: any) {
    console.info('appService, updateUser');
    return this._userService.updateUser(userId, data);
  }
}
