import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as fs from 'fs';
import { Model } from 'mongoose';
import { Summary, SummaryDocument } from '../schemas/summary.schema';

@Injectable()
export class FilesManagerService {
  constructor(
    @InjectModel(Summary.name) private _summaryModel: Model<SummaryDocument>,
  ) {}
  private months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  private async _processFile(
    file: string,
    userId: string,
    saveSummary: boolean,
  ) {
    const formatData = file
      .split('\n')
      .map((e) => e.trim())
      .map((e) => e.split(','));

    return await this._filterAndFixData(
      this._createTransactionsObject(formatData),
      userId,
      saveSummary,
    );
  }

  private _createTransactionsObject = (formatData: Array<any>) =>
    formatData.map((transaction) => ({
      id: transaction[0],
      date: transaction[1],
      transaction: +transaction[2],
    }));

  private async _filterAndFixData(
    formatData,
    userId: string,
    saveSummary: boolean,
  ) {
    const agroupedByMonth = [];

    for (let i = 1; i <= 12; i++) {
      const byMonth = formatData.filter((transaccion) => {
        const month = +transaccion.date.split('/')[1];
        return month === i;
      });
      byMonth.length ? agroupedByMonth.push(byMonth) : null;
    }
    const newData = agroupedByMonth.map((data) => {
      return {
        month: this._getMonthLabel(data[0]),
        averageDebitAmount: this._getAverageDebitAmount(data),
        averageCreditAmount: this._getAverageCreditAmount(data),
        totalTransactions: data.length,
        transactions: [...data],
      };
    });

    const summaryData = {
      totalBalance: this._getTotalBalance(formatData),
      totalTransactions: formatData.length,
      data: newData,
    };

    if (saveSummary) {
      const created = await this._saveSummaryData(userId, summaryData);
      const createdSummaryData = {
        _id: created._id.valueOf(),
        ...summaryData,
      };
      return createdSummaryData;
    }

    return summaryData;
  }

  private _getMonthLabel(data) {
    const month = +data.date.split('/')[1];
    return this.months[month - 1];
  }

  private _getTotalBalance(data) {
    return data.reduce(
      (acc, newValue) => +(acc + newValue.transaction).toFixed(2),
      0,
    );
  }

  private _getAverageDebitAmount(data) {
    const transactions = this._filterTransactionsByType(data, 'debit');
    const average = this._getTransactionsAverage(transactions);
    return average;
  }

  private _getAverageCreditAmount(data) {
    const transactions = this._filterTransactionsByType(data, 'credit');
    const average = this._getTransactionsAverage(transactions);
    return average;
  }

  private _filterTransactionsByType(data, type) {
    return data.filter((dataTransaction) =>
      type === 'debit'
        ? dataTransaction.transaction < 0
        : dataTransaction.transaction > 0,
    );
  }

  private _getTransactionsAverage(transactions) {
    const total = +transactions.reduce(
      (acum, newValue) => +(acum + newValue.transaction).toFixed(2),
      0,
    );
    return +(total / transactions.length).toFixed(2);
  }

  private async _saveSummaryData(userId: string, summaryDataDto) {
    const summaryData = new this._summaryModel({
      user: userId,
      ...summaryDataDto,
    });
    return summaryData.save();
  }

  public async processSummaryData(userId: string, saveSummary: boolean) {
    try {
      const dir = `files-data/${userId}.csv`;
      const data = fs.readFileSync(dir, 'utf8');
      return await this._processFile(data.toString(), userId, saveSummary);
    } catch (e) {
      throw new Error(e);
    }
  }

  public getSummaryData(summaryId: string) {
    console.info('files manager service, getSummaryData');
    return this._summaryModel.findById(summaryId).exec();
  }
}
