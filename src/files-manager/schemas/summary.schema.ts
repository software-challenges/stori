import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SummaryDocument = Summary & Document;

@Schema()
export class Summary {
  @Prop()
  user: string;

  @Prop()
  totalBalance: number;

  @Prop()
  totalTransactions: number;

  @Prop()
  data: Array<MonthTransaction>;
}

interface MonthTransaction {
  month: string;
  averageDebitAmount: number;
  averageCreditAmount: number;
  totalTransactions: number;
  data: Array<Transaction>;
}

interface Transaction {
  id: string;
  date: string;
  transaction: number;
}

export const SummarySchema = SchemaFactory.createForClass(Summary);
