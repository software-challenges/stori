import { Module } from '@nestjs/common';
import { FilesManagerService } from './services/files-manager.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Summary, SummarySchema } from './schemas/summary.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Summary.name, schema: SummarySchema }]),
  ],
  providers: [FilesManagerService],
  exports: [FilesManagerService],
})
export class FilesManagerModule {}
