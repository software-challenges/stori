import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { UsersService } from '../../users/services/user.service';
import { FilesManagerService } from 'src/files-manager/services/files-manager.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class EmailService {
  constructor(
    private _usersService: UsersService,
    private _filesManagerService: FilesManagerService,
    private _mailerService: MailerService,
    private _configService: ConfigService,
  ) {}
  async sendSummaryEmailInformation(userId: string, saveSummary: boolean) {
    console.info('EmailService, sendSummaryEmailInformation');
    const user = await this._usersService.getUser(userId);
    const summaryData = await this._filesManagerService.processSummaryData(
      userId,
      saveSummary,
    );
    //send email
    await this._mailerService.sendMail({
      to: user.email,
      subject: 'Summary transactions',
      template: './summary',
      context: {
        logo: this._configService.get<string>('images.logo'),
        frontPage: this._configService.get<string>('images.frontPage'),
        name: `${user.name} ${user.lastName}`,
        totalBalance: summaryData.totalBalance,
        totalTransactions: summaryData.totalTransactions,
        transactions: summaryData.data,
      },
    });

    return {
      user,
      summaryData,
    };
  }
}
