import { Module } from '@nestjs/common';
import { EmailService } from './services/email.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { join } from 'path';
import { UsersModule } from '../users/users.module';
import { FilesManagerModule } from '../files-manager/files-manager.module';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    UsersModule,
    FilesManagerModule,
    MailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          transport: {
            host: configService.get<string>('smtp.host'),
            port: configService.get<number>('smtp.port'),
            ignoreTLS: true,
            secure: true,
            auth: {
              user: configService.get<string>('smtp.mail'),
              pass: configService.get<string>('smtp.pass'),
            },
          },
          defaults: {
            from: '"No Reply Stori"',
          },
          template: {
            dir: join(__dirname, 'templates'),
            adapter: new HandlebarsAdapter(undefined, {
              inlineCssEnabled: true,
              inlineCssOptions: {
                url: ' ',
                preserveMediaQueries: true,
              },
            }),
            options: {
              strict: true,
            },
          },
        };
      },
    }),
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
