import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../schemas/user.schema';
import { Model } from 'mongoose';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}
  getUser(id: string) {
    console.info('UsersService, getUser');
    return this.userModel.findById(id).exec();
  }

  updateUser(id: string, data) {
    console.info('UsersService, updateUser');
    return this.userModel.findByIdAndUpdate(id, data, { new: true });
  }
}
