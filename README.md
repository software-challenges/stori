# Stori Challenge

## Description

This project read and process a .csv file to send an email about summary transactions

## Cloning the app

```bash

$ git clone https://gitlab.com/software-challenges/stori.git

```
## Install dependencies

In project's root

```bash

$ npm install

```

## Running the app

### 1. Create a .env file with the following params:
   * STORI_LOGO_URL
   * STORI_FRONT_PAGE_URL
   * HOST
   * PORT
   * MAIL
   * PASS
   * MONGODB_URL
  
  A .env file will be include in the email details

### 2. Run Watch mode

This command start up the application on port 3000 (http://localhost:3000/)

```bash
$ npm run start:dev
```
This command will create a docker image from root project and will start up a docker container on port 80 (http://localhost:80/)

## Running with docker-compose
```bash
$ npm run start:stori-challenge
```

## Using the app

The app has 4 functionalities

1. Get users information
2. Update user information
3. Get summary data
4. Send summary email


The project is deploy in a EC2

To process and send email, first you need to update the user email 

The .csv file is include in the files-data folder in the projects root



A postman collection will be include in the email with the diferents enviroments and you will be able to diference them wth the words LOCAL and AWS
