FROM node:14

WORKDIR /app

COPY package.json .

COPY ./files-data ./files-data

RUN npm install

RUN npm build

COPY ./dist ./src

CMD ["node", "src/main.js"]
